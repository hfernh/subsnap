# Subsnap

[Subsnap](https://gitlab.com/hfernh/subsnap) is a script to make local
snapshots of [btrfs](https://btrfs.wiki.kernel.org/index.php/Main_Page)
subvolumes for the purpose of having local backups.


## Feature overview

- creates a labeled and timestamped readonly snapshot of a btrfs
  subvolume in a specified directory
- includes house keeping, retaining only a specified number of the same
  labeled snapshots
- can be called from crontabs, or anacron


## Use case

Assume a local disk `/dev/sda`, with one parttion `/dev/sda1`, and formatted
under btrfs having a subvolume `rootfs` for the linux root file system,
It would also have a directoy `snaps` for the snapshots.

The whole disk would be mounted under `/mnt/sda1`.

This would be mounted in `/etc/fstab` as:

```
# <fs> <mountpoint> <opts> <dump> <pass>
/dev/sda1 / btrfs noatime,subvol=rootfs 0 1
/dev/sda1 /mnt/sda1 btrfs noatime 0 0
```

With this script in `root/bin`, daily, weekly and monthy snapshops could be
created from root's crontab as follows:

```
# Mins  Hours  Days   Months  Day of the week
10 1 * * *  /root/bin/subsnap /mnt/sda1/rootfs /mnt/sda1/snaps daily 21
15 1 * * 1  /root/bin/subsnap /mnt/sda1/rootfs /mnt/sda1/snaps weekly 7
20 1 1 * *  /root/bin/subsnap /mnt/sda1/rootfs /mnt/sda1/snaps monthly 18
```

This would create 21 daily snapshots, 7 weekly and 18 monthly
in `/mnt/sda1/snaps`.

## Subsnaps
This is a supplementary script to create multiple snaps of multiple sources
with one command.
It assumes hourly, daily, weekly, monthly and yearly labels.

- Hourly snapshots are at the top of the hour
- Daily at midnight
- Weekly at midnight from Sunday to Monday
- Monthly at midnight at the fist day of the month
- Yearly at midnight on New Years day


It can be called from root's cron once an hour as follows:

```
# Mins  Hours  Days   Months  Day of the week
0 * * * *  /root/bin/subsnaps /mnt/sda1/{rootfs,home,data} /mnt/sda1/snaps 25 32 5 13 10
```

This would make / retain:

- 25 hourly snaphots for /mnt/sda1/rootfs, /mnt/sda1/home, and /mnt/sda1/data
- 32 daily snaphots for /mnt/sda1/rootfs, /mnt/sda1/home, and /mnt/sda1/data
- 5 weekly snaphots for /mnt/sda1/rootfs, /mnt/sda1/home, and /mnt/sda1/data
- 13 monthly snaphots for /mnt/sda1/rootfs, /mnt/sda1/home, and /mnt/sda1/data
- 10 yearly snaphots for /mnt/sda1/rootfs, /mnt/sda1/home, and /mnt/sda1/data


# Warning

Subsnap does not protect against harddisk failures as it makes shallow copies
on the same physical disk.


# License

Subsnap and subsnaps are licensed under the
[MIT license](https://gitlab.com/hfernh/subsnap/-/blob/master/LICENSE).


# Feedback

If you have an enhancement request, or something is not working properly then
please log an [issue](https://gitlab.com/hfernh/subsnap/-/issues) against
the project.
